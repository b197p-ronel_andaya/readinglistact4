//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/

const http = require("http");
port = 8000;

const server = http.createServer(function (request, response) {
  if (request.url == "/" && request.method == 'GET') {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome to Ordering System");
  } else if (request.url == "/dashboard" && request.method == 'GET') {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end(`Welcome to your User's Dashboard!`);
  } else if (request.url == "/products") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end(`Here’s our products available`);
  } else if (request.url == "/addProduct" && request.method == 'POST') {
    response.writeHead(200, { "Content-Type": "application/json" });
    response.end("Add a course to our resources");
  }else if(request.url == "/updateProduct" && request.method == 'PUT'){
    response.writeHead(200, { "Content-Type": "application/json" });
    response.end("Update a course to our resources");
  }else if(request.url == "/archiveProduct" && request.method == 'DELETE'){
    response.writeHead(200, { "Content-Type": "application/json" });
    response.end("Archive courses to our resources");
  }else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.end("Sorry! The page you are looking for cannot be found.");
  }
});

server.listen(port);

console.log(`Server is now up and running and listening on port ${port}`);